package Exception;


public class ContaCadastrada extends Exception {

    public long numero;

    public ContaCadastrada (long n) {
        super ("Conta ja existente!");
        numero = n;
    }

    public long getNumero() {
            return numero;
    }
}
