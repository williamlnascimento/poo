package Exception;

public class SaldoInsuficiente extends Exception {

    private long numero;
    private double saldo;

    public SaldoInsuficiente (double s, long n) {
        super ("Saldo Insuficiente!");
        saldo = s;
        numero = n;
    }

    public double getSaldo() {
            return saldo;
    }

    public long getNumero() {
            return numero;
    }
}
