package Exception;

public class ValoresPositivos extends Exception {

    public ValoresPositivos() {
        super("Os valores devem ser positivos!");
    }
     
    public String getMensagem(){
        return "Os valores devem ser positivos!";
    }

    
}
