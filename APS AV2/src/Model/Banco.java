package Model;


import java.util.*;

import Model.*;
import Exception.*;

public class Banco {
    
    private ArrayList<ContaBancaria> Contas;
    
    public Banco(){
        if (Contas == null){
            Contas = new ArrayList<>();
        }
    }
    
    public List<ContaBancaria> getContas(){
        return Contas;
    }
    
    public void inserir(ContaBancaria conta) throws ContaCadastrada{
        for (ContaBancaria col : Contas){
            if (col.getNumeroConta() == conta.getNumeroConta()){
                throw new ContaCadastrada(col.getNumeroConta());
            }
        }
        Contas.add(conta);
    }
    public void remover(long numeroConta) throws ContaInexistente, ValoresPositivos{
        if (numeroConta <0) {
            throw new ValoresPositivos();
        }
         ContaBancaria conta = procurarConta(numeroConta);
         if (conta == null){
             throw new ContaInexistente(numeroConta);
         }else{
             for (ContaBancaria col : Contas){
                if (col.getNumeroConta() == conta.getNumeroConta()){
                    Contas.remove(conta);
                    break;
                }
             }
         }
    }
    public ContaBancaria procurarConta(long numeroConta) throws ContaInexistente, ValoresPositivos{
         if (numeroConta <0) {
            throw new ValoresPositivos();
        }
         for (ContaBancaria conta : Contas){
            if (conta.getNumeroConta() == numeroConta){
              return conta;
            }
        }
         throw new ContaInexistente(numeroConta);
    } 
    
    
}
