package Model;

import Exception.SaldoInsuficiente;
import Exception.ValoresPositivos;

public class ContaCorrente extends ContaBancaria implements Imprimivel {

    private double limite;

    public double getLimite() {
            return limite;
    }

    public ContaCorrente(long numeroConta,
                            double saldo,
                            double limite)  throws ValoresPositivos{
            super(numeroConta, saldo);
            this.limite = limite;
    }
    
    public void sacar(double valor) throws SaldoInsuficiente, ValoresPositivos{            
            if (valor <= getSaldo()+getLimite()){
                    super.sacar(valor);
            }else{
               throw new SaldoInsuficiente(valor, this.getNumeroConta());
            }
    }
    @Override
    public void depositar(double valor) throws ValoresPositivos{
             super.depositar(valor); //REUSO!!!
    } 
    @Override   
    public String mostrarDados() {
        return "CONTA: "+getNumeroConta()+"          | R$: "+getSaldo()+"          | Lim: "+getLimite();
    }
	
}
