package Model;

import Exception.*;
public class ContaPoupanca extends ContaBancaria implements Imprimivel{
        
    private double TaxaDeOperacao;

    public double getTaxaDeOperacao() {
        return TaxaDeOperacao*100;
    }

    public ContaPoupanca(long numeroConta,
            double saldo,
            double TaxaDeOperacao)  throws ValoresPositivos{
        super(numeroConta, saldo);
        this.TaxaDeOperacao = TaxaDeOperacao/100;
    }
    
    @Override
    public void sacar(double valor) throws SaldoInsuficiente, ValoresPositivos {
            double valorTaxado = valor+(valor*this.TaxaDeOperacao);
            if (valorTaxado <= getSaldo()) {
                    super.sacar(valorTaxado); 
            }else{
                throw new SaldoInsuficiente(valorTaxado, getNumeroConta());
            }
    }
    @Override
   public void depositar(double valor) throws ValoresPositivos {
            super.depositar(valor-(valor*this.TaxaDeOperacao)); 
            } 

    @Override
    public String mostrarDados() {
        return "CONTA: "+getNumeroConta()+"          | R$: "+getSaldo()+"          | TAXA: "+getTaxaDeOperacao();
    }
    
}
