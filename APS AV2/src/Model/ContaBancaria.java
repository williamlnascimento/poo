package Model;

import Exception.SaldoInsuficiente;
import Exception.ValoresPositivos;

public abstract class ContaBancaria implements Imprimivel{
    
        private long numeroConta;
        private double saldo;

        public double getSaldo() {
            return saldo;
        }
        public long getNumeroConta() {
            return numeroConta;
        }
        public  void sacar(double valor) throws SaldoInsuficiente, ValoresPositivos{
             if (valor <0) {
                throw new ValoresPositivos();
            }
            this.saldo -= valor;
        } 
        public void depositar(double valor) throws ValoresPositivos{
             if (valor <0) {
                throw new ValoresPositivos();
            }
            this.saldo += valor;
        } 
        public void transferir(double valor, ContaBancaria conta) throws SaldoInsuficiente , ValoresPositivos{
            this.sacar(valor);
            conta.depositar(valor);
        } 
        public ContaBancaria(long numeroConta, double valor)  throws  ValoresPositivos{
             if (numeroConta <0) {
                throw new ValoresPositivos();
            }
            this.numeroConta = numeroConta;
            this.depositar(valor);
        } 
        
        
}
