import java.util.*;
import java.math.*;

public class Exercicio_1 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
		
		double raio;
		double area;
		
		System.out.println("***Calculando area do circulo***");
		System.out.println("Digite o raio do circulo: ");
		raio = tecla.nextDouble();
		
		area = 3.14159 * (Math.pow(raio,2));
		
		System.out.println("Valor da area �: " + area);

	}

}


/*
	1) Escreva um programa para ler o raio de um c�rculo, calcular e escrever a sua �rea
*/