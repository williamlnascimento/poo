import java.util.*;

public class Exercicio_1_outraSelecao {

	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
		
		int codigoOrigem;
		
		System.out.println("Digite o numero de origem do produto: ");
		codigoOrigem = tecla.nextInt();
		
		switch(codigoOrigem) {
			case 1:{
				System.out.println("Regi�o de sua proced�ncia: Sul");
				break;
			}
			case 2:{
				System.out.println("Regi�o de sua proced�ncia: Norte");
				break;
			}
			case 3:{
				System.out.println("Regi�o de sua proced�ncia: Leste");				
				break;
			}
			case 4:{
				System.out.println("Regi�o de sua proced�ncia: Oeste");				
				break;
			}
			case 5: case 6:{
				System.out.println("Regi�o de sua proced�ncia: Nordeste");
				break;
			}
			case 7: case 8: case 9:{
				System.out.println("Regi�o de sua proced�ncia: Sudeste");
				break;
			}
			case 10:{
				System.out.println("Regi�o de sua proced�ncia: Centro-Oeste");
				break;
			}
			case 11:{
				System.out.println("Regi�o de sua proced�ncia: Noroeste");
				break;
			}
			default:
				System.out.println("Regi�o de sua proced�ncia: Importado");
	
}
}
}

/*
 1) Escreva um programa que leia o c�digo de origem de um produto e imprima na tela a regi�o de sua
	proced�ncia conforme a tabela abaixo:
	c�digo 1 : Sul 
	c�digo 2 : Norte 
	c�digo 3 : Leste 
	c�digo 4 : Oeste 
	c�digo 5 ou 6 : Nordeste
	c�digo 7, 8 ou 9 : Sudeste
	c�digo 10 : Centro-Oeste
	c�digo 11 : Noroeste	
	Observa��o: Caso o c�digo n�o seja nenhum dos especificados o produto deve ser encarado como
	Importado.

*/
