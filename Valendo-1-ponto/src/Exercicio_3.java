import java.util.*;

public class Exercicio_3 {

	public static void main(String[] args) {
		
		Scanner tecla = new Scanner(System.in);
		double celsius;
		double fahrenheit;
		
		System.out.println("***Calcular Celsius Para Fahrenheit***");
		System.out.println("Digite o Valor em Celsius: ");
		celsius = tecla.nextDouble();
		
		fahrenheit = (celsius * 9/5) + 32;
		
		System.out.println("Valor em Fahrenheit �: " + fahrenheit + "�F");
	}

}


/*	3) Escreva um programa para ler uma temperatura em graus Celsius, calcular e escrever o valor
	correspondente em graus Fahrenheit.
*/