import java.util.*;

public class Exerciocio_2 {

	public static void main(String[] args) {
			
			double fahrenheit;
			double celsius;
			
			Scanner tecla = new Scanner(System.in);
			
			System.out.println("***Calcular Fahrenheit Para Celsius***");
			System.out.println("Digite o Valor em Fahrenheit: ");
			fahrenheit = tecla.nextDouble();
			
			celsius = (fahrenheit - 32) * 5/9;

			System.out.println("Valor em Celsius �: " + celsius + "�C");

		}
		
	}

/*
	2) Escreva um programa para ler uma temperatura em graus Fahrenheit, calcular e escrever o valor
	correspondente em graus Celsius.
*/