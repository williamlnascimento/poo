import java.util.*;

public class Exercicio_4 {

	public static void main(String[] args) {
		Scanner tecla = new Scanner(System.in);
		int potencia;
		int qtdMetros;
		int qtdLampada;
		
		System.out.println("Digite a potencia das lampadas: ");
		potencia = tecla.nextInt();
		
		System.out.println("Digite quantos metros quadrados possui o comodo: ");
		qtdMetros = tecla.nextInt();
		
		qtdLampada = (qtdMetros * 18) / potencia;
		
		
		System.out.println("Quantidade de lampadas necessarias: " + qtdLampada);
	}

}


/*4) Escreva um programa para calcular e imprimir o n�mero de l�mpadas necess�rias para iluminar um
determinado c�modo de uma resid�ncia. Dados de entrada: a pot�ncia da l�mpada utilizada (em
watts), as dimens�es (largura e comprimento, em metros) do c�modo. Considere que a pot�ncia
necess�ria � de 18 watts por metro quadrado.*/
