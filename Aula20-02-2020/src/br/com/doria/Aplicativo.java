package br.com.doria;

import java.util.*;

public class Aplicativo {

public static void main(String[] args) {
	//declaração de variavel
	double av1, av2, media;
	Scanner tecla = new Scanner(System.in);
	
	//Entrada de dados
	System.out.println("Digite AV1: ");
	av1 = tecla.nextDouble();
	System.out.println("Digite AV2: ");
	av2 = tecla.nextDouble();
	
	//Processamento de dados
	media = (av1+av2)/2;
	
	//Saida de informação
	if(media > 7){
		
		System.out.println("Aprovado");
	}else{
		System.out.println("Reprovado");
	}
	}
}
